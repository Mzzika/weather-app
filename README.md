## Architecture & used libraries

The micro single page application was built using **HTML, jQuery & Bootstrap**
The data was fetched from **OpenWeatherMap** which has a straight forward to use API.
I used **Google fonts** in order to import two fonts that may fit with the design I made.
All the links are CDN to ensure a faster page load.
I used **animate.css** a very effective UI library for CSS transitions and effects.
I wanted to make the UI look even better, by showing a photo on the background of the card in each city called from the API; for that,
I used the free API for STOCK photos provided by **unsplash.com**, the photos keep changing randomly in each call which makes the UI looks even better.
I used **HighCharts** in order to make a chart that shows the weather forecast in the next hours on a particular city and I've used CSS to style it and
let it fit the design of the page.


## How to improve it

If I was free to chose the best way to make this micro SPA, I would definitely build it using **Angular 2** as it'll be cleaner and even easier to maintain.
However, I've tried to stick to instructions and make it show only by clicking on `index.html` as in the instructions it is clearly written that ** Please make sure your exercise runs simply by opening your main HTML file in a browser **
