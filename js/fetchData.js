var getCitiesUrl = "http://api.openweathermap.org/data/2.5/group?id=2759794,2988507,3067696,2267057,2643743&units=metric&appid=eaeb7b552b8a44a56e670ddd8cecf1c1";

var getData = $.getJSON(getCitiesUrl, function(data) {

    data.list.forEach(cityDetails => {

      var cityId = cityDetails.id;
      var cityName = cityDetails.name;
      var avgTemp = cityDetails.main.temp.toFixed(1);
      var windSpeed = cityDetails.wind.speed;
      var photoOfTheCity = "https://source.unsplash.com/category/buildings/?";
      var fetchedCityPhoto = photoOfTheCity + cityName;

      var cardToAppend = `<div class="col" id="` + cityId + `">
            <div class="card card-inverse" style="background-image: url('` + fetchedCityPhoto + `');">
              <div class="card-block layer">
              <div class="verticalAlign">
                <h6 class="cityName">` + cityName + `</h6>
                 <h1 class="temp">` + avgTemp + `</h1>
                 <span class="degreeSymbol">&#8451</span><br>
                 <span class="windSpeed">Wind speed ` + windSpeed + ` m/s</span><br>
                <button id="` + cityId + `" class="btn btn-primary btnPosition">MORE</button>
              </div>
              </div>
            </div>
          </div>`;

      //Append the fethed city info
      $("#block").append(cardToAppend);

      //Pass the cityId & cityName arguments to getForecast
      $("#" + cityId).on('click', (function() {
        getForecast(cityId, cityName, fetchedCityPhoto);
        $("#chart").replaceWith('<div id="container" class="chart"></div>');
      }));

    });
  })
  .fail(function() {
    console.log("error");
  });


//Get forecast of a particular city

function getForecast(cityId, cityName, fetchedCityPhoto) {

  var forecastCityUrl = 'http://api.openweathermap.org/data/2.5/forecast?id=' + cityId + '&units=metric&appid=eaeb7b552b8a44a56e670ddd8cecf1c1';

  var getData = $.getJSON(forecastCityUrl, function(data) {

      //Limited to the next 6 time slot, in each slot 3 hours
      var nextFiveSlotsArray = data.list.slice(0, 5);
      var city = data.city.name;
      var timeSlotsArray = [];
      var upcommingTempArray = [];

      var nextHoursForecastCard = `<div class="col animated flipInX" id="` + cityId + `">
                <div class="card card-inverse" style="background-image: url('` + fetchedCityPhoto + `');">
                  <div class="card-block layer">
                  <div class="verticalAlign">
                    <h6 class="cityName">` + city + `</h6>
                    <div class="container-fluid">
                    <div class="row equal bottom">
                    </div>
                    </div>
                  </div>
                  </div>
                </div>
              </div>`;

      $("div#" + cityId).html(nextHoursForecastCard);

      nextFiveSlotsArray.forEach(nextHoursForecast => {

        var upcommingTemp = parseInt(nextHoursForecast.main.temp.toFixed(1));
        var dateTime = new Date(nextHoursForecast.dt_txt);

        var hours = dateTime.getHours()
        var minutes = dateTime.getMinutes();
        minutes = minutes > 9 ? minutes : '0' + minutes;

        var time = hours + ':' + minutes;
        timeSlotsArray.push(time);
        upcommingTempArray.push(upcommingTemp);

        plotValuesInChart(city, timeSlotsArray, upcommingTempArray);
        $('#container').addClass('animated slideInUp');

        //After clicking "See more" button you will see the forecast weather of the upcomming hours
        var showWeatherDegrees = `<div class="col">
           <h6 class="smallTemp"> ` +
          time + `</h6>
           <h6 class="smallTemp"> ` +
          upcommingTemp + `&#8451 </h6>
           </div>`;

        $('.equal', "#" + cityId).append(showWeatherDegrees);

      });

    })
    .fail(function() {
      console.log("error");
    });
}
///
